import java.util.ArrayList;
public class Gen <T>  {
    ArrayList<T> arrli = new ArrayList<>();
    void add(T obj) throws Exc {
        if (obj==null){
            throw new Exc("Empty");
        }else if(arrli.contains(obj)){
            throw new Exc("Duplicate");
        }else {
            arrli.add(obj);
        }
    }
    T get(int i){
        return arrli.get(i);
    }
}
